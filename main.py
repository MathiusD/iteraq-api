from flask import Flask, jsonify, request,Response
from core.fix.infix import infix
from core.fix.postfix import postfix
from core.secret.secret import secret
from core.matrix.matrix import matrix
import base64
app=Flask(__name__)
app.config["DEBUG"]=True

@app.route('/greetings',methods=['GET'])
def test():
    message = "hello, "
    if 'name' in request.args:
        if request.args['name'] =="":
            return Response(status=400)
        message = message + request.args['name']
        print(request.args['name'])
    else:
        message= message + "world"
    return jsonify(message=message)

@app.route('/greetings',methods=['POST'])
def POST():
    message = "hello, "
    if 'name' in request.json:
        if request.json['name'] =="":
            return Response(status=400)

        message = message + request.json['name']
        
    else:
        message= message + "world"
    return jsonify(message=message)

@app.route('/expense-report',methods=['POST'])
def infix2():
    if 'type' in request.args:
        plop = postfix((str(request.data).replace("b'", "").replace("'", "")))
        if plop =="error":
            return Response(status=400)
        return jsonify(value=plop)
    else:
        plop = infix((str(request.data).replace("b'", "").replace("'", "")))
        if plop =="error":
            return Response(status=400)
        return jsonify(value=plop)

@app.route('/secret-base64',methods=['POST'])
def secret_post():
    if 'to-translate' in request.json:
        if request.json['to-translate'] =="":
            return jsonify(text="")
        plop = base64.b64decode(request.json['to-translate'])
        return jsonify(text=str(plop).replace("b'", "").replace("'", ""))

@app.route('/secret-reduced',methods=['POST'])
def reduce_post():
    if 'to-translate' in request.json:
        if request.json['to-translate'] =="":
            return jsonify(text="")
        plop = secret(request.json['to-translate'])
        return jsonify(text=str(plop).replace("b'", "").replace("'", ""))

@app.route('/drawing',methods=['POST'])
def matrix2():
    if 'width' in request.args and 'height' in request.args:
        plop=matrix(str(request.data).replace("b'", "").replace("'", ""),int(request.args["width"]),int(request.args["height"]))
    else:
        plop=matrix(request.json["payload"])
    if plop=='error':
        return Response(status=400)
    return jsonify(image=str(plop).replace("b'", "").replace("'", ""))


app.run(port=3000)