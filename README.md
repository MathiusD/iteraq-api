# IterαQ - Section API

Ce dépôt est le dépôt consacré au développement de l'api l'équipe IterαQ au sein de la nightcode 2020.

## Configuration/Installation

Afin de configurer cette section au mieux, voici une liste d'action que nous n'avons pas réussi à automatiser via le makefile :
* Placer le fichier 000-default.conf au sein de votre configuration appache dans un sous-dossier `enabled_sites/`

## Lancement/Execution

Afin de lancer l'api nous n'avons pas réussi à automatiser via le makefile voici donc les instructions:
* zsh
* source env/bin/activate
* python3 main.py

## Auteurs

* Maxime Leriche
* Lucas Gambier
* Féry Mathieu
